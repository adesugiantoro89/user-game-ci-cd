'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game_biodata.belongsTo(models.user_games, {foreignKey: 'userGameId'})
      // user_game_biodata.hasOne(models.user_games, {foreignKey: 'userGameId'})

    }
  }
  user_game_biodata.init({
    userGameId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    tanggal_lahir: DataTypes.DATE,
    tempat_lahir: DataTypes.STRING,
    alamat: DataTypes.TEXT,
    telp: DataTypes.STRING,
    video_perkenalan: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'user_game_biodata',
  });
  return user_game_biodata;
};