'use strict';
const bcrypt = require('bcryptjs');

const {
  Model
} = require('sequelize');

// const caesar_encrypt = require('../encrypt')
module.exports = (sequelize, DataTypes) => {
  class user_games extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_games.hasOne(models.user_game_biodata, {foreignKey: 'userGameId'})
      // user_games.belongsTo(models.user_game_biodata, {foreignKey: 'id'})


    }
  }
  user_games.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_games',
  });

  user_games.addHook('beforeCreate',(user,option) => {
    // console.log(user_games);
   
      // user_games.password = caesar_encrypt(user_games.password, 2)
      const salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(user.password, salt);
  
  },
  user_games.addHook('beforeUpdate',(user,option)=> {
  
      // user_games.password = caesar_encrypt(user_games.password, 2)
      const salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(user.password, salt);

  })
  )
  return user_games;
};