'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_historys extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user_game_historys.init({
    userGamesId: DataTypes.INTEGER,
    skor: DataTypes.INTEGER,
    waktu: DataTypes.TIME
  }, {
    sequelize,
    modelName: 'user_game_historys',
  });
  return user_game_historys;
};