const request = require('supertest')
const {
    sequelize
} = require('../models/index')
const {
    queryInterface
} = sequelize
const bcrypt = require('bcryptjs')
const app = require('../app')

beforeEach(() => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("adesug123", salt)
    queryInterface.bulkInsert('user_games', [
        {
        username: "adesug",
        email : "adesugian@gmail.com",
        password: hash,
        createdAt: new Date(),
        updatedAt: new Date()
    }
])
})

afterEach(async () => {
    await queryInterface.bulkDelete('user_games', {}, {
        truncate: true,
        restartIdentity: true
    })
})

describe('Login API', () => {
    it('Success', (done) => {
      request(app)
      .post('/user-login')
      .send({
        username: "adesug",
        password: "adesug123"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else { 
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('token')
          done()
        }
      })
    })
  
    it('Wrong password', (done) => {
      request(app)
      .post('/user-login')
      .send({
        username: "adesug",
        password: "Qweqwe"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Invalid username or password')
          done()
        }
      })
    })
  
    it('Wrong username', (done) => {
      request(app)
      .post('/user-login')
      .send({
        username: "bagas",
        password: "adesug123"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Invalid username or password')
          done()
        }
      })
    })
  })

  describe ('create user', () => {
    it('Success', (done) => {
        request(app)
        .post('/user-registration')
        .send({
            username: "adesug",
            email: "adesugiantoro89@gmail.com",
            password: "adesug123"
        })
        .end((err, res) => {
            if(err) {
                done(err)
            }else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Succes register')
                done() 
            }
        })
    })
})