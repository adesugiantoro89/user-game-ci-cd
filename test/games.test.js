const request = require('supertest')
const {
    sequelize
} = require('../models/index')
const {
    queryInterface
} = sequelize
const bcrypt = require('bcryptjs')
const app = require('../app')

beforeEach(() => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("adesug123", salt)
    queryInterface.bulkInsert('user_games', [{
        username: "adesug",
        email: "adesugian@gmail.com",
        password: hash,
        createdAt: new Date(),
        updatedAt: new Date()
    }])
})

afterEach(async () => {
    await queryInterface.bulkDelete('user_games', {}, {
        truncate: true,
        restartIdentity: true
    })
})

describe('List User', () => {
    it('Success', (done) => {
        request(app)
            .get('/user_games')

            .end((err, res) => {
                if (err) {
                    done(err)
                } else {
                    expect(res.status).toBe(200)
                    expect(res.body).toHaveProperty('data')
                    done()
                }
            })
    }) 
})

describe ('create user', () => {
    it('Success', (done) => {
        request(app)
        .post('/user_games')
        .send({
            username: "adesug",
            email: "adesugiantoro89@gmail.com",
            password: "adesug123"
        })
        .end((err, res) => {
            if(err) {
                done(err)
            }else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Succesfully create user')
                done() 
            }
        })
    })
})
describe('Get Users By ID', () => {
    it('Success', (done) => {
        request(app)
        .get('/user_games/3')
        .end((err,res) => {
            if(err) {
                done(err)
            }else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('tes')
                done()
            }
        })
    })
})

describe('Delete Users', () => {
    it('Success', (done) => {
        request(app)
        .delete('/user_games/4')
        .end((err,res) => {
            if(err) {
                done(err)
            }else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('data')
                done()
            }
        })
    })
})

