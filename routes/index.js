const express = require('express')
const router = express.Router()
const authRoutes = require('./auth.route')
const biodata = require('./biodata.route')

router.use('/auth', authRoutes)
router.use('/app', biodata)

module.exports = router