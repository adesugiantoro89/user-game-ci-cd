const express = require('express');
const router = express.Router()
const BiodataController = require('../controllers/bidata.user.controller');
const storage = require('../helpers/multerstorage')
const multer = require('multer');
const upload = multer({
    storage,
    limits: {
        fileSize: 10000000
    },
    fileFilter: (req, file, cb) => {
        if (file.mimetype === 'video/mp4' || file.mimetype === 'video/mkv' || file.mimetype === 'video/mwv') {
          cb(null, true)
        } else {
          cb({
            status: 400,
            message: 'File type does not match'
          }, false)
        }
      }
})

router.post('/user-biodata',upload.single('video_perkenalan'), BiodataController.postBiodata)
router.get('/user-biodata', BiodataController.getBiodata)

module.exports = router