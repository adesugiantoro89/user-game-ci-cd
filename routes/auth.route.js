const express = require('express');
const router = express.Router()
const AuthController = require('../controllers/auht.user.controller')

router.post('/user-registration', AuthController.register);
router.post('/user-login', AuthController.loginPassword);
router.post('/user-login-email', AuthController.loginGoogle);
router.post('/user-login-facebook', AuthController.loginFacebook);

module.exports = router