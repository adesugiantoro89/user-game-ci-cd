# user game contract

### API CONTRACT

### POST /user-login

### Success

- Request

```json
{
    "username" : "adesug",
    "password" : "adesug123"
}
```

- Response 200

```json
{
    "token": "${string}"
}
```

#### Wrong Password

- Request
  
```json
{
    "username": "adesug",
    "password" : "adesug1"
}
```
-Response (401)
```json
{
    "message":"Invalid username or password"
}
```

#### Wrong Username

- Request
  
```json
{
    "username": "adesug_",
    "password" : "adesug123"
}
```
-Response (401)
```json
{
    "message":"Invalid username or password"
}
```

### post-register

### success

- Request

```json
{
    "username" : "adesug",
    "email" : "adesugiantoro89@gmail.com",
    "password" : "adesug123"
}
```

### Get-list data

- Request
```json
{
    "id" : 1,
    "username" : "adesug",
    "email" : "adesugiantoro89@gmail.com",
    "password" : "adesug123"
},
{
    "id" : 2,
    "username" : "adesug",
    "email" : "adesugiantoro89@gmail.com",
    "password" : "adesug123"
},

```

### Get by id

- Request/1
```json
{
    "id" : 1,
    "username" : "adesug",
    "email" : "adesugiantoro89@gmail.com",
    "password" : "adesug123"
},
```

### create

- Request
```json
{
    
    "username" : "adesug",
    "email" : "adesugiantoro89@gmail.com",
    "password" : "adesug123"
},
```

### Delete

- Request
```json
{
   "message" : "Data Terhapus"
},
```

### Update

- Request
```json
{
   "message" : "Data Diupdate"
},
```





