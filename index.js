const express = require('express')
const app = express()
const port = 3000
const routes = require('./routes/index')
const errorHandler = require('./helpers/errorHandler');
const morgan = require('morgan')

// const port = process.env.PORT

const userGameController = require('./controllers/controller');


const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use(express.urlencoded({extended: false}))
app.use(express.json())
app.use(morgan('dev'))

app.use(express.static('public'))
app.use(routes)

app.get('/user_games', userGameController.list)
app.get('/user_games/:id',userGameController.getById)
app.delete('/user_games/:id',userGameController.delete)
app.put('/user_games/:id',userGameController.update)

app.use(errorHandler)

app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })