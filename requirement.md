# User Game Integration Testing

## App Requirement 

- Register User
  - username,email,password wajib diisi
  - response sukses memberikan message "Success Create User"
  
- Login User
  - memasukkan username dan password
  - Reponse sukses memberikan token (200)
  - Password salah memberikan error (401)
  - Username Salah memberikan error  (401)

- Get All UserGame
  - Response (200) Sukses
    - Menampilkan all user (id,username,email,password)
- Get By ID 
  - Response (200) sukses
    - Menampilkan username,email,password
  - Response (404) tidak ada data
    - Menampilkan Message "Data Not Found" 
- Delete 
  - Response (200) sukses
    - menampilkan pesan data terhapus
  - Response (404) not found
    - data tidak ditemukan
- Update
  - Response (200) sukses
    - menampilkan data diupdate    
 
  