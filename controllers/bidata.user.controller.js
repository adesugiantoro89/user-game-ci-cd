const {user_game_biodata,user_games} = require('../models/index')

class BiodataUserGame {
    static async postBiodata(req,res,next) {
       
        try {
            // console.log(req.file)
            if(req.file) {
                req.body.video_perkenalan = `http://localhost:3000/${req.file.filename}`
            }
           let data = await user_game_biodata.create({
                userGameId : req.body.userGameId,
                name : req.body.name,
                tanggal_lahir : req.body.tanggal_lahir,
                tempat_lahir : req.body.tempat_lahir,
                alamat : req.body.alamat,
                telp : req.body.telp,
                video_perkenalan : req.body.video_perkenalan
            })
            res.status(200).json({
                message: 'Biodata berhasil dilengkapi',
                data : data
            })
        } catch (error) {
            next(error)
        }
    }
    static async getBiodata(req,res,next) {
        try {
            let tes = await user_game_biodata.findAll({
                include: {
                    model: user_games,
                  }
            }
                
            )
            res.status(200).json(tes)
        } catch (err) {
            next(err)
        }
    }
}

module.exports = BiodataUserGame