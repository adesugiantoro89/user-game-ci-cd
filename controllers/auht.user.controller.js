const {user_games} = require('../models/index')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)
const axios = require('axios')

class userControllerAuth {
    static async register (req,res,next){
        try {
            await user_games.create({
                username: req.body.username,
                password: req.body.password,
                email:req.body.email
            })
            res.status(200).json({
                message:'Succes register'
            })
        } catch (error) {
            next(err)
        }
    }
    static async loginPassword (req,res,next) {
        try {
            const user = await user_games.findOne({
                where: {
                    username:req.body.username
                }
            })
           
            if (!user) {
                throw {
                    status : 401,
                    message: 'Invalid username or password'
                }
            }else{
                if(bcrypt.compareSync(req.body.password, user.password)) {
                    let token = jwt.sign({
                        id: user.id, 
                        username: user.username
                    }, 'plugin')

                    res.status(200).json({
                        token
                    })
                    console.log(token);
                }else{
                    throw{
                        status:401,
                        message: 'Invalid username or password'
                    }
                }
            }
        } catch (err) {
            next(err)
            
        }
    }
    static async loginGoogle(req,res,next) {
        try {
            const token = await client.verifyIdToken({
                idToken: req.body.google_id_token,
                audience: process.env.GOOGLE_CLIENT_IDs

            })
            // console.log(token);
            const payload = token.getPayload()
            // res.status(200).json(payload.email)
            const user = await  user_games.findOne({
                where: {
                  email: payload.email
                }
              })
              if (user) {
                const token = jwt.sign({
                  id: user.id,
                  email: user.email
                }, 'plugin')
                res.status(200).json({ token })
              }


        } catch (err) {
            next(err)
        }
    }
    static async loginFacebook(req,res,next) {
        try {
            const response = await axios.get(`https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${req.body.token}`)

            // res.status(200).json(response.data)
            const user = await user_games.findOne({
                where: {
                  email: response.data.email
                }
              })
              if (user) {
                const token = jwt.sign({
                  id: user.id,
                  email: user.email
                }, 'plugin')
                res.status(200).json({ token })
              }
        } catch (err) {
            next(err)
        }
    }
    
}

module.exports = userControllerAuth


