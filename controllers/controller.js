const {user_games} = require('../models')
const user_game_biodata = require('../models/user_game_biodata')
class userGameController {
    static async list(req,res) {
      try {
       let data = await  user_games.findAll({
          attributes: ['id', 'username', 'email','password'],
        })
          res.status(200).json({data})    
      } catch (error) {
        res.status(500).json({
          message : "Internal Server Error"
        })
      }
       
        
    }
    static async getById(req,res) {
     try {
      let tes =  await user_games.findOne({
        where: {
            id:req.params.id
        },
       
        
    })
    
      res.status(200).json({tes})
    
   
     } catch (err) {
      res.status(500).json({
        message: 'Internal Server Error',
        
      })
     }
           
     
        
    }
   
      static update(req,res) {
        user_games.update({
          username : req.body.username
        }, {
          where: {
            id:req.params.id
          },
          individualHooks:true, 
          returning:true,
        })
        .then((data) => {
          console.log(data);
          res.status(200).json({
            message: "Data Diupdate",
            data,
          })
        })
        .catch((error) => {
          res.status(500).json(error)
        })
      }
    
      static async delete (req, res) {
        try {
         let data = await user_games.destroy({
            where: {
              id: req.params.id
            }
          })
          
            res.status(200).json({
              data,
              message: 'Data terhapus'
            })
          
        } catch (error) {
          res.status(500).json({
            message: 'Internal Server Error'
          })
        }
      }
}

module.exports = userGameController