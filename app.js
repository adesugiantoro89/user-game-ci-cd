const express = require ('express')
const app = express()
const port = 3000

const userControllerAuth = require('./controllers/user.controller');
const userGameController = require('./controllers/controller');



const errorHandler = require('./errorHandler');


const { body, validationResult } = require('express-validator');

app.use(express.urlencoded({extended: false}))
app.use(express.json())


app.post('/user-registration',
[
  body('username').notEmpty(),
  body('password').notEmpty(),
  body('email').notEmpty()
],
(req,res,next) => {
  const errors = validationResult(req);
  if(!errors.isEmpty()) {
    throw {
      status : 400,
      message: errors.array() 
    }
  }
  else{
    next()
  }
},userControllerAuth.register)

app.post('/user-login',
[
  body('username').notEmpty(),
  body('password').notEmpty()
],
(req,res,next) => {
  const errors = validationResult(req);
  if(!errors.isEmpty()) {
    throw {
      status : 400,
      message: errors.array() 
    }
  }
  else{
    next()
  }
},userControllerAuth.login);

app.get('/user_games', userGameController.list)
app.post('/user_games',userGameController.create)
app.get('/user_games/:id',userGameController.getById)
app.delete('/user_games/:id',userGameController.delete)




app.use(errorHandler)

module.exports = app