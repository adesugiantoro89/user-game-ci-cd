'use strict';
const bcrypt = require("bcryptjs");

module.exports = {
  async up (queryInterface, Sequelize) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync("1234567890", salt);

    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('user_games', [
     {
     username: 'ade_sug',
     email : 'adesugiantoro89@gmail.com',
     password : hash,
     createdAt: new Date(),
     updatedAt: new Date()
    },
    {
      username: 'aufa_alaina',
      email : 'alaina@gmail.com',
      password : hash,
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      username: 'rizal_khamam',
      email : 'rizal@gmail.com',
      password : hash,
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      username: 'user_86',
      email : 'user@gmail.com',
      password : hash,
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      username: 'bagas_7',
      email : 'bagas@gmail.com',
      password : hash,
      createdAt: new Date(),
      updatedAt: new Date()
     },
  ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('user_games', null, {truncate:true, restartIdentity:true});
  }
};
