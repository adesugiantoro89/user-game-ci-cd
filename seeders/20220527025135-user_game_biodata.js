'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('user_game_biodata',[
     {
       userGameId: '1',
       name: 'Ade Sugiantoro',
       tanggal_lahir: '2000-01-01',
       tempat_lahir: 'Tegal',
       alamat: 'Kota Tegal Jawa Tengah',
       telp: '0895367517829',
       video_perkenalan: "http://localhost:3000/video_perkenalan-1653641648248-WhatsApp Video 2022-05-27 at 15.36.43.mp4",
       createdAt: new Date(),
      updatedAt: new Date()
     }
   ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('user_game_biodata',null,{truncate:true, restartIdentity:true});
  }
};
